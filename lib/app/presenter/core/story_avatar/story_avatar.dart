import 'package:flutter/material.dart';
import 'package:instagram_clone/app/models/domain/user.dart';
import 'package:instagram_clone/app/presenter/core/user_avatar.dart';

part 'add_story_button.dart';
part 'live_status_container.dart';

class StoryAvatar extends StatelessWidget {
  const StoryAvatar({
    Key? key,
    required User user,
    required bool isSelfStory,
    required double dimension,
    bool useSimplifiedView = true,
  })  : _user = user,
        _isSelfStory = isSelfStory,
        _dimension = dimension,
        _useSimplifiedView = useSimplifiedView,
        super(key: key);

  ///
  final User _user;

  ///
  final bool _isSelfStory;

  ///
  final double _dimension;

  /// If `true`, only shows the avatar and the story border
  final bool _useSimplifiedView;

  @override
  Widget build(BuildContext context) {
    final userAvatar = UserAvatar(
      user: _user,
      dimension: _dimension - UserAvatar.borderWidth,
    );

    Widget storyWidget = userAvatar;

    if (_user.hasStory) {
      storyWidget = Stack(
        alignment: Alignment.center,
        children: [
          SizedBox.square(
            dimension: _dimension,
            child: const DecoratedBox(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                gradient: LinearGradient(
                  colors: [Colors.yellow, Colors.red, Colors.purple],
                  begin: Alignment.bottomLeft,
                  end: Alignment.topRight,
                ),
              ),
            ),
          ),
          storyWidget,
        ],
      );
    } else {
      storyWidget = SizedBox.square(
        dimension: _dimension,
        child: storyWidget,
      );
    }

    if (_isSelfStory) {
      const double margin = UserAvatar.borderWidth;
      storyWidget = Stack(
        alignment: Alignment.center,
        children: [
          storyWidget,
          const Positioned(
            right: margin,
            bottom: margin,
            child: _AddStoryButton(),
          ),
        ],
      );
    } else {
      if (!_useSimplifiedView && _user.isLive) {
        final height = _dimension * 0.225;
        storyWidget = Stack(
          clipBehavior: Clip.none,
          alignment: Alignment.center,
          children: [
            storyWidget,
            Positioned(
              bottom: -UserAvatar.borderWidth,
              child: _LiveStatusContainer(
                width: _dimension * 0.4,
                height: height,
              ),
            ),
          ],
        );
      }
    }

    return storyWidget;
  }
}

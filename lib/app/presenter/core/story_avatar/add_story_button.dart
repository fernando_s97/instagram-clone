part of 'story_avatar.dart';

class _AddStoryButton extends StatefulWidget {
  static const double _circleSize = 20.0;
  static const double _plusSize = _AddStoryButton._circleSize * 0.8;

  const _AddStoryButton({Key? key}) : super(key: key);

  @override
  State<_AddStoryButton> createState() => _AddStoryButtonState();
}

class _AddStoryButtonState extends State<_AddStoryButton> {
  late ThemeData _themeData;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _themeData = Theme.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: _AddStoryButton._circleSize,
      height: _AddStoryButton._circleSize,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
          color: _themeData.scaffoldBackgroundColor,
          width: 1.5,
        ),
      ),
      child: CircleAvatar(
        radius: _AddStoryButton._circleSize,
        child: const Icon(Icons.add, size: _AddStoryButton._plusSize),
        backgroundColor: _themeData.colorScheme.secondary,
        foregroundColor: _themeData.colorScheme.onSecondary,
      ),
    );
  }
}

part of 'story_avatar.dart';

class _LiveStatusContainer extends StatelessWidget {
  static const double _borderWidth = UserAvatar.borderWidth / 3;

  const _LiveStatusContainer({
    Key? key,
    required double width,
    required double height,
  })  : _width = width,
        _height = height,
        super(key: key);

  final double _width;
  final double _height;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: _width,
      height: _height,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(
          color: Theme.of(context).scaffoldBackgroundColor,
          width: _borderWidth,
        ),
        gradient: const LinearGradient(
          colors: [Colors.pink, Colors.red],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
      ),
      child: const Center(
        child: Text(
          'LIVE',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 6,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

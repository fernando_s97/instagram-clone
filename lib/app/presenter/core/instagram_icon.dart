import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:instagram_clone/app/models/instagram_icon_data.dart';

class InstagramIcon extends StatelessWidget {
  const InstagramIcon(
    InstagramIconData iconData, {
    Key? key,
    Size? size,
    Color? color,
    AlignmentGeometry? alignment,
  })  : _iconData = iconData,
        _size = size,
        _color = color,
        _alignment = alignment ?? Alignment.center,
        super(key: key);

  final InstagramIconData _iconData;
  final Size? _size;
  final Color? _color;
  final AlignmentGeometry _alignment;

  @override
  Widget build(BuildContext context) {
    final assetPath = _getAssetPath();

    final IconThemeData iconTheme = Theme.of(context).iconTheme;

    final Color? color = _color ?? iconTheme.color;

    final iconThemeSize = iconTheme.size;
    final Size? size =
        _size ?? (iconThemeSize == null ? null : Size.square(iconThemeSize));

    final isSvg = assetPath.endsWith('.svg');
    if (isSvg) {
      return SvgPicture.asset(
        assetPath,
        color: color,
        width: size?.width,
        height: size?.height,
      );
    } else {
      return Image.asset(
        assetPath,
        color: color,
        width: size?.width,
        height: size?.height,
        alignment: _alignment,
      );
    }
  }

  String _getAssetPath() {
    final String assetName;

    switch (_iconData) {
      case InstagramIconData.commentOutline:
        assetName = 'comment_icon_outline.png';
        break;
      case InstagramIconData.home:
        assetName = 'home_icon.png';
        break;
      case InstagramIconData.homeOutline:
        assetName = 'home_icon_outline.png';
        break;
      case InstagramIconData.like:
        assetName = 'like_icon.png';
        break;
      case InstagramIconData.likeOutline:
        assetName = 'like_icon_outline.png';
        break;
      case InstagramIconData.messengerOutline:
        assetName = 'messenger_icon_outline.png';
        break;
      case InstagramIconData.newPostOutline:
        assetName = 'new_post_icon_outline.png';
        break;
      case InstagramIconData.reels:
        assetName = 'reels_icon.png';
        break;
      case InstagramIconData.reelsOutline:
        assetName = 'reels_icon_outline.png';
        break;
      case InstagramIconData.save:
        assetName = 'save_icon.png';
        break;
      case InstagramIconData.saveOutline:
        assetName = 'save_icon_outline.png';
        break;
      case InstagramIconData.search:
        assetName = 'search_icon.png';
        break;
      case InstagramIconData.searchOutline:
        assetName = 'search_icon_outline.png';
        break;
      case InstagramIconData.shareOutline:
        assetName = 'share_icon_outline.png';
        break;
      case InstagramIconData.shop:
        assetName = 'shop_icon.png';
        break;
      case InstagramIconData.shopOutline:
        assetName = 'shop_icon_outline.png';
        break;
      case InstagramIconData.logotype:
        assetName = 'logotype.png';
        break;
    }

    return 'assets/images/$assetName';
  }
}

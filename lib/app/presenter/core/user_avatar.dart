import 'package:flutter/material.dart';
import 'package:instagram_clone/app/models/domain/user.dart';

@immutable
class UserAvatar extends StatelessWidget {
  static const double borderWidth = 4;

  const UserAvatar({
    Key? key,
    required User user,
    required double dimension,
    Color? borderColor,
    Gradient? borderGradient,
  })  : _user = user,
        _dimension = dimension,
        assert(borderColor == null || borderGradient == null),
        _borderColor = borderColor,
        _borderGradient = borderGradient,
        super(key: key);

  final User _user;
  final double _dimension;
  final Color? _borderColor;
  final Gradient? _borderGradient;

  @override
  Widget build(BuildContext context) {
    final profileImage = _user.profileImage;
    final avatarDimension = _dimension - borderWidth;

    Widget avatarImage;
    if (profileImage == null) {
      avatarImage = SizedBox.square(
        dimension: avatarDimension,
        child: DecoratedBox(
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.grey,
          ),
          child: Icon(Icons.person, size: _dimension * 0.7),
        ),
      );
    } else {
      avatarImage = SizedBox.square(
        dimension: avatarDimension,
        child: DecoratedBox(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(image: NetworkImage(profileImage)),
          ),
        ),
      );
    }

    const baseDecoration = BoxDecoration(shape: BoxShape.circle);
    Decoration effectiveDecoration;
    if (_borderColor != null) {
      effectiveDecoration = baseDecoration.copyWith(color: _borderColor);
    } else if (_borderGradient != null) {
      effectiveDecoration = baseDecoration.copyWith(gradient: _borderGradient);
    } else {
      effectiveDecoration = baseDecoration.copyWith(
        color: Theme.of(context).scaffoldBackgroundColor,
      );
    }

    final avatarWidget = Stack(
      alignment: Alignment.center,
      children: [
        SizedBox.square(
          dimension: _dimension,
          child: DecoratedBox(decoration: effectiveDecoration),
        ),
        avatarImage,
      ],
    );

    return avatarWidget;
  }

  UserAvatar copyWith({
    User? user,
    double? size,
    Color? borderColor,
    Gradient? borderGradient,
  }) {
    return UserAvatar(
      user: user ?? _user,
      dimension: size ?? _dimension,
      borderColor: borderColor ?? _borderColor,
      borderGradient: borderGradient ?? _borderGradient,
    );
  }
}

import 'package:flutter/material.dart';
import 'package:instagram_clone/app/presenter/core/coming_soon.dart';

class ReelsPage extends StatelessWidget {
  const ReelsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Reels')),
      body: const ComingSoon(),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:instagram_clone/app/presenter/core/coming_soon.dart';

class ShopPage extends StatelessWidget {
  const ShopPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Shop')),
      body: const ComingSoon(),
    );
  }
}

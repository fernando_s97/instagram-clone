import 'package:flutter/material.dart';
import 'package:instagram_clone/app/models/domain/user.dart';
import 'package:instagram_clone/app/models/instagram_icon_data.dart';
import 'package:instagram_clone/app/presenter/core/instagram_icon.dart';
import 'package:instagram_clone/app/presenter/core/user_avatar.dart';
import 'package:instagram_clone/app/presenter/explore/explore_page.dart';
import 'package:instagram_clone/app/presenter/home/home_page.dart';
import 'package:instagram_clone/app/presenter/profile/credits/credits_page.dart';
import 'package:instagram_clone/app/presenter/reels/reels_page.dart';
import 'package:instagram_clone/app/presenter/shop/shop_page.dart';

part '../models/bottom_nav_bar_content.dart';

class BottomNavBarContainer extends StatefulWidget {
  final User _loggedInUser;

  const BottomNavBarContainer({Key? key, required User loggedInUser})
      : _loggedInUser = loggedInUser,
        super(key: key);

  @override
  _BottomNavBarContainerState createState() => _BottomNavBarContainerState();
}

class _BottomNavBarContainerState extends State<BottomNavBarContainer> {
  List<_BottomNavBarContent> _items = [];

  int _selectedTabIndex = 0;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _initItems();
  }

  void _initItems() {
    const double userAvatarSize = 28;

    _items = [
      _BottomNavBarContent(
        pageContent: HomePage(loggedInUser: widget._loggedInUser),
        navBarContent: const BottomNavigationBarItem(
          label: 'Home',
          icon: InstagramIcon(InstagramIconData.homeOutline),
          activeIcon: InstagramIcon(InstagramIconData.home),
        ),
      ),
      const _BottomNavBarContent(
        pageContent: ExplorePage(),
        navBarContent: BottomNavigationBarItem(
          label: 'Explore',
          icon: InstagramIcon(InstagramIconData.searchOutline),
          activeIcon: InstagramIcon(InstagramIconData.search),
        ),
      ),
      const _BottomNavBarContent(
        pageContent: ReelsPage(),
        navBarContent: BottomNavigationBarItem(
          label: 'Reels',
          icon: InstagramIcon(InstagramIconData.reelsOutline),
          activeIcon: InstagramIcon(InstagramIconData.reels),
        ),
      ),
      const _BottomNavBarContent(
        pageContent: ShopPage(),
        navBarContent: BottomNavigationBarItem(
          label: 'Shop',
          icon: InstagramIcon(InstagramIconData.shopOutline),
          activeIcon: InstagramIcon(InstagramIconData.shop),
        ),
      ),
      _BottomNavBarContent(
        pageContent: const CreditsPage(),
        navBarContent: BottomNavigationBarItem(
          label: 'Profile',
          icon: UserAvatar(
            user: widget._loggedInUser,
            dimension: userAvatarSize,
          ),
          activeIcon: UserAvatar(
            user: widget._loggedInUser,
            dimension: userAvatarSize,
            borderColor: Theme.of(context).iconTheme.color,
          ),
        ),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _selectedTabIndex,
        children: _items.map((e) => e.pageContent).toList(),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedTabIndex,
        items: _items.map((e) => e.navBarContent).toList(),
        onTap: (index) => setState(() => _selectedTabIndex = index),
      ),
    );
  }
}

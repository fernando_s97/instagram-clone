import 'package:flutter/material.dart';
import 'package:instagram_clone/app/models/domain/comment.dart';
import 'package:instagram_clone/app/models/domain/post.dart';
import 'package:instagram_clone/app/models/domain/user.dart';
import 'package:instagram_clone/app/models/instagram_icon_data.dart';
import 'package:instagram_clone/app/presenter/core/instagram_icon.dart';
import 'package:instagram_clone/app/presenter/core/story_avatar/story_avatar.dart';
import 'package:instagram_clone/app/presenter/core/user_avatar.dart';

part 'posts_list/post_widget/medias/media_count_chip.dart';
part 'posts_list/post_widget/medias/post_medias.dart';
part 'posts_list/post_widget/post_comments.dart';
part 'posts_list/post_widget/post_header.dart';
part 'posts_list/post_widget/post_widget.dart';
part 'posts_list/posts_list.dart';
part 'stories_list.dart';

class HomePage extends StatefulWidget {
  static const double _defaultPaddingValue = 8;

  const HomePage({Key? key, required User loggedInUser})
      : _loggedInUser = loggedInUser,
        super(key: key);

  final User _loggedInUser;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late double _screenHeight;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _screenHeight = MediaQuery.of(context).size.height;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: InstagramIcon(
          InstagramIconData.logotype,
          size: Size.fromHeight(_screenHeight * 0.05),
          alignment: Alignment.centerLeft,
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: const InstagramIcon(InstagramIconData.newPostOutline),
          ),
          IconButton(
            onPressed: () {},
            icon: const InstagramIcon(InstagramIconData.likeOutline),
          ),
          IconButton(
            onPressed: () {},
            icon: const InstagramIcon(InstagramIconData.messengerOutline),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: _screenHeight * 0.16,
              child: _StoriesList(
                loggedInUser: widget._loggedInUser,
                followingList: widget._loggedInUser.followingList
                    .where((element) => element.hasStory)
                    .toList(),
              ),
            ),
            const SizedBox(height: HomePage._defaultPaddingValue),
            const Divider(),
            _PostsList(loggedInUser: widget._loggedInUser),
          ],
        ),
      ),
    );
  }
}

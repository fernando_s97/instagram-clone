part of '../../../app/presenter/home/home_page.dart';

class _StoriesList extends StatefulWidget {
  // [left padding, user story, following stories]
  static const int _elementsBeforeFollowingStoriesCount = 2;

  _StoriesList({
    Key? key,
    required User loggedInUser,
    required List<User> followingList,
  })  : _loggedInUser = loggedInUser,
        assert(
          !followingList.any((element) => !element.hasStory),
          'All following users provided MUST have story',
        ),
        _followingList = followingList,
        super(key: key);

  final User _loggedInUser;
  final List<User> _followingList;

  @override
  State<_StoriesList> createState() => _StoriesListState();
}

class _StoriesListState extends State<_StoriesList> {
  late int _allElementsCount;

  @override
  void initState() {
    super.initState();

    _allElementsCount = _computeAllElementsCount();
  }

  @override
  void didUpdateWidget(covariant _StoriesList oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (oldWidget._followingList != widget._followingList) {
      _allElementsCount = _computeAllElementsCount();
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      scrollDirection: Axis.horizontal,
      itemCount: _allElementsCount,
      padding: const EdgeInsets.only(right: HomePage._defaultPaddingValue),
      itemBuilder: (context, index) {
        final isLeftPadding = index == 0;
        if (isLeftPadding) {
          return const SizedBox(
            width: HomePage._defaultPaddingValue,
          );
        }

        final User user;

        final isSelfStory = index == 1;
        if (isSelfStory) {
          user = widget._loggedInUser;
        } else {
          // "Fix" offset caused by left padding and self story widgets.
          final fixedUserIndex =
              index - _StoriesList._elementsBeforeFollowingStoriesCount;
          user = widget._followingList[fixedUserIndex];
        }

        return SizedBox(
          width: 70,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              StoryAvatar(
                user: user,
                dimension: 64,
                isSelfStory: isSelfStory,
                useSimplifiedView: false,
              ),
              const SizedBox(height: 5),
              Text(
                isSelfStory ? 'Your story' : user.nickname,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(fontSize: 12),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        );
      },
      separatorBuilder: (_, index) {
        final isLeftPaddingSeparator = index == 0;
        if (isLeftPaddingSeparator) return const SizedBox();

        return const SizedBox(width: HomePage._defaultPaddingValue);
      },
    );
  }

  int _computeAllElementsCount() =>
      _StoriesList._elementsBeforeFollowingStoriesCount +
      widget._followingList.length;
}

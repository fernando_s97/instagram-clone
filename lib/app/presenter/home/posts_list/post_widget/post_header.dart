part of '../../home_page.dart';

class _PostHeader extends StatelessWidget {
  const _PostHeader({
    Key? key,
    required User ownerUser,
    required User loggedInUser,
  })  : _ownerUser = ownerUser,
        _loggedInUser = loggedInUser,
        super(key: key);

  final User _ownerUser;
  final User _loggedInUser;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        StoryAvatar(
          user: _ownerUser,
          dimension: 34,
          isSelfStory: _ownerUser == _loggedInUser,
        ),
        const SizedBox(width: 6),
        Text(_ownerUser.nickname),
        const Spacer(),
        IconButton(onPressed: () {}, icon: const Icon(Icons.more_vert)),
      ],
    );
  }
}

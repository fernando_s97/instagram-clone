part of '../../../home_page.dart';

class _MediaCountChip extends StatelessWidget {
  const _MediaCountChip({
    Key? key,
    required int currentPostIndex,
    required int postCount,
  })  : _currentPostIndex = currentPostIndex,
        _postCount = postCount,
        super(key: key);

  final int _currentPostIndex;
  final int _postCount;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30),
        color: Theme.of(context).scaffoldBackgroundColor.withOpacity(.8),
      ),
      child: Center(
        child: Text('${_currentPostIndex + 1}/$_postCount'),
      ),
    );
  }
}

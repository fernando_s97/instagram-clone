part of '../../../home_page.dart';

class _PostMedias extends StatefulWidget {
  const _PostMedias({Key? key, required this.post, required this.ownerUser})
      : super(key: key);

  final Post post;
  final User ownerUser;

  @override
  State<_PostMedias> createState() => _PostMediasState();
}

class _PostMediasState extends State<_PostMedias> {
  late final PageController _controller;

  late double _mediasHeight;

  late int _currentMediaIndex;

  @override
  void initState() {
    _controller = PageController(initialPage: 0);
    _currentMediaIndex = 0;

    super.initState();
  }

  @override
  void didChangeDependencies() {
    _mediasHeight = MediaQuery.of(context).devicePixelRatio * 160;

    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: _mediasHeight,
          child: Stack(
            children: [
              PageView(
                scrollDirection: Axis.horizontal,
                controller: _controller,
                onPageChanged: (index) =>
                    setState(() => _currentMediaIndex = index),
                children: widget.post.mediasUrl.map((mediaUrl) {
                  return Image.network(
                    mediaUrl,
                    loadingBuilder: (_, child, progress) {
                      if (progress == null) return child;

                      return const Center(child: CircularProgressIndicator());
                    },
                    errorBuilder: (_, __, ___) => const Center(
                      child: Icon(Icons.error),
                    ),
                  );
                }).toList(),
              ),
              if (_controller.hasClients && widget.post.mediasUrl.length >= 2)
                Positioned(
                  top: 20,
                  right: 15,
                  child: _MediaCountChip(
                    currentPostIndex: _currentMediaIndex,
                    postCount: widget.post.mediasUrl.length,
                  ),
                ),
            ],
          ),
        ),
        Row(
          children: [
            IconButton(
              onPressed: () {},
              icon: const InstagramIcon(InstagramIconData.likeOutline),
            ),
            IconButton(
              onPressed: () {},
              icon: const InstagramIcon(InstagramIconData.commentOutline),
            ),
            IconButton(
              onPressed: () {},
              icon: const InstagramIcon(InstagramIconData.shareOutline),
            ),
            const Spacer(),
            IconButton(
              onPressed: () {},
              icon: const InstagramIcon(InstagramIconData.saveOutline),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: HomePage._defaultPaddingValue,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text(
                '${widget.post.likes} likes',
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 2),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: '${widget.ownerUser.nickname} ',
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
                    TextSpan(text: widget.post.caption),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

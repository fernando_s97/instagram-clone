part of '../../home_page.dart';

class _PostComments extends StatefulWidget {
  const _PostComments({
    Key? key,
    required this.comments,
    required this.loggedInUser,
  }) : super(key: key);

  final List<Comment> comments;
  final User loggedInUser;

  @override
  State<_PostComments> createState() => _PostCommentsState();
}

class _PostCommentsState extends State<_PostComments> {
  late TextTheme _textTheme;
  late TextStyle? _elapsedTimeTextStyle;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _textTheme = Theme.of(context).textTheme;
    _elapsedTimeTextStyle = _textTheme.caption?.copyWith(fontSize: 10);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (widget.comments.isNotEmpty)
          Padding(
            padding: const EdgeInsets.symmetric(
              vertical: _PostWidget._defaultVerticalSpace,
            ),
            child: Text(
              'View all ${widget.comments.length} comments',
              style: _textTheme.caption,
            ),
          )
        else
          const SizedBox(height: _PostWidget._defaultVerticalSpace),
        Row(
          children: [
            UserAvatar(user: widget.loggedInUser, dimension: 28),
            const SizedBox(width: _PostWidget._addCommentSpaceBetween),
            Expanded(
              child: TextField(
                decoration: InputDecoration.collapsed(
                  hintText: 'Add a comment',
                  hintStyle: TextStyle(fontSize: _textTheme.caption?.fontSize),
                ),
                maxLines: 1,
              ),
            ),
            const SizedBox(width: _PostWidget._addCommentSpaceBetween),
            const Text('❤️', style: TextStyle(fontSize: 15)),
            const SizedBox(width: _PostWidget._addCommentSpaceBetween),
            const Text('🙌', style: TextStyle(fontSize: 15)),
            const SizedBox(width: _PostWidget._addCommentSpaceBetween),
            const Icon(Icons.add_circle_outline, size: 15),
          ],
        ),
        const SizedBox(height: _PostWidget._addCommentSpaceBetween),
        RichText(
          text: TextSpan(
            style: _elapsedTimeTextStyle,
            children: [
              const TextSpan(text: '29 minutes ago \u{2022} '),
              TextSpan(
                text: 'See translation',
                style: _elapsedTimeTextStyle?.copyWith(
                  color: _textTheme.bodyText2?.color,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

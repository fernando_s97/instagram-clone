part of '../../home_page.dart';

class _PostWidget extends StatelessWidget {
  static const double _defaultVerticalSpace = 7;
  static const double _addCommentSpaceBetween = 7;

  const _PostWidget({
    Key? key,
    required this.post,
    required this.ownerUser,
    required this.loggedInUser,
  }) : super(key: key);

  final Post post;
  final User ownerUser;
  final User loggedInUser;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(
            left: HomePage._defaultPaddingValue,
          ),
          child: _PostHeader(
            ownerUser: ownerUser,
            loggedInUser: loggedInUser,
          ),
        ),
        _PostMedias(post: post, ownerUser: ownerUser),
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: HomePage._defaultPaddingValue,
          ),
          child: _PostComments(
            comments: post.comments,
            loggedInUser: loggedInUser,
          ),
        ),
      ],
    );
  }
}

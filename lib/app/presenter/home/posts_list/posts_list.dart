part of '../home_page.dart';

class _PostsList extends StatefulWidget {
  final User _loggedInUser;

  const _PostsList({Key? key, required User loggedInUser})
      : _loggedInUser = loggedInUser,
        super(key: key);

  @override
  _PostsListState createState() => _PostsListState();
}

class _PostsListState extends State<_PostsList> {
  late List<Widget> _postWidgetsList;

  @override
  void initState() {
    super.initState();

    _postWidgetsList = _mapPostsToWidget();
  }

  @override
  void didUpdateWidget(covariant _PostsList oldWidget) {
    super.didUpdateWidget(oldWidget);

    _postWidgetsList = _mapPostsToWidget();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (_, index) => _postWidgetsList[index],
      separatorBuilder: (_, index) {
        return const SizedBox(height: HomePage._defaultPaddingValue * 2);
      },
      itemCount: _postWidgetsList.length,
    );
  }

  List<Widget> _mapPostsToWidget() {
    final List<Widget> widgets = [];

    for (final user in widget._loggedInUser.followingList) {
      for (final post in user.posts) {
        widgets.add(_PostWidget(
          post: post,
          ownerUser: user,
          loggedInUser: widget._loggedInUser,
        ));
      }
    }

    return widgets;
  }
}

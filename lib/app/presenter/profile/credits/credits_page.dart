import 'package:flutter/material.dart';
import 'package:marquee/marquee.dart';
import 'package:url_launcher/url_launcher.dart';

part '../../../models/credits.dart';

class CreditsPage extends StatelessWidget {
  static const _credits = <_CreditListTile>[
    _CreditListTile(
      title: 'Instagram icon set vector image',
      author: 'Atok',
      url:
          'https://www.vectorstock.com/royalty-free-vector/instagram-icon-set-vector-21718006',
      description: 'Image #21718006 at VectorStock.com',
    ),
    _CreditListTile(
      title: 'Coming Soon GIF',
      author: 'Propertymatchmakers',
      url:
          'https://giphy.com/gifs/propertymatchmakers-coming-soon-wvlYFEv9hx9p6KuKRA',
    ),
  ];

  const CreditsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Credits')),
      body: ListView.builder(
        itemCount: _credits.length,
        itemBuilder: (_, index) => _credits[index],
      ),
    );
  }
}

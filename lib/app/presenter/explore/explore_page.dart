import 'package:flutter/material.dart';
import 'package:instagram_clone/app/presenter/core/coming_soon.dart';

class ExplorePage extends StatelessWidget {
  const ExplorePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Explore')),
      body: const ComingSoon(),
    );
  }
}

part of '../presenter/profile/credits/credits_page.dart';

// https://opensource.stackexchange.com/questions/1727/how-to-attribute-creative-commons-images-in-a-quiz-game-mobile-app
class _CreditListTile extends StatelessWidget {
  const _CreditListTile({
    Key? key,
    required this.title,
    required this.author,
    required this.url,
    this.description,
    this.license,
  }) : super(key: key);

  final String title;
  final String author;
  final String url;
  final String? description;
  final String? license;

  @override
  Widget build(BuildContext context) {
    bool isThreeLine = false;

    final subtitleBuilder = StringBuffer();

    if (description != null) {
      subtitleBuilder.write(description);
    }

    if (license != null) {
      if (subtitleBuilder.isNotEmpty) {
        subtitleBuilder.write('\n');
        isThreeLine = true;
      }

      subtitleBuilder.write('Licensed under $license');
    }

    final subtitle =
        subtitleBuilder.isEmpty ? null : Text(subtitleBuilder.toString());

    return ListTile(
      title: SizedBox(
        height: 20,
        child: Marquee(text: '$title by $author'),
      ),
      isThreeLine: isThreeLine,
      subtitle: subtitle,
      onTap: _launchURL,
    );
  }

  void _launchURL() async =>
      await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
}

part of '../presenter/bottom_nav_bar_container.dart';

@immutable
class _BottomNavBarContent {
  final Widget pageContent;
  final BottomNavigationBarItem navBarContent;

  const _BottomNavBarContent({
    required this.pageContent,
    required this.navBarContent,
  });
}

// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'user.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$UserTearOff {
  const _$UserTearOff();

  _User call(
      {required String nickname,
      String? profileImage,
      bool hasStory = false,
      bool isLive = false,
      List<User> liveWith = const [],
      List<User> followingList = const [],
      List<Post> posts = const []}) {
    return _User(
      nickname: nickname,
      profileImage: profileImage,
      hasStory: hasStory,
      isLive: isLive,
      liveWith: liveWith,
      followingList: followingList,
      posts: posts,
    );
  }
}

/// @nodoc
const $User = _$UserTearOff();

/// @nodoc
mixin _$User {
  String get nickname => throw _privateConstructorUsedError;
  String? get profileImage => throw _privateConstructorUsedError;
  bool get hasStory => throw _privateConstructorUsedError;
  bool get isLive => throw _privateConstructorUsedError;
  List<User> get liveWith => throw _privateConstructorUsedError;
  List<User> get followingList => throw _privateConstructorUsedError;
  List<Post> get posts => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $UserCopyWith<User> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserCopyWith<$Res> {
  factory $UserCopyWith(User value, $Res Function(User) then) =
      _$UserCopyWithImpl<$Res>;
  $Res call(
      {String nickname,
      String? profileImage,
      bool hasStory,
      bool isLive,
      List<User> liveWith,
      List<User> followingList,
      List<Post> posts});
}

/// @nodoc
class _$UserCopyWithImpl<$Res> implements $UserCopyWith<$Res> {
  _$UserCopyWithImpl(this._value, this._then);

  final User _value;
  // ignore: unused_field
  final $Res Function(User) _then;

  @override
  $Res call({
    Object? nickname = freezed,
    Object? profileImage = freezed,
    Object? hasStory = freezed,
    Object? isLive = freezed,
    Object? liveWith = freezed,
    Object? followingList = freezed,
    Object? posts = freezed,
  }) {
    return _then(_value.copyWith(
      nickname: nickname == freezed
          ? _value.nickname
          : nickname // ignore: cast_nullable_to_non_nullable
              as String,
      profileImage: profileImage == freezed
          ? _value.profileImage
          : profileImage // ignore: cast_nullable_to_non_nullable
              as String?,
      hasStory: hasStory == freezed
          ? _value.hasStory
          : hasStory // ignore: cast_nullable_to_non_nullable
              as bool,
      isLive: isLive == freezed
          ? _value.isLive
          : isLive // ignore: cast_nullable_to_non_nullable
              as bool,
      liveWith: liveWith == freezed
          ? _value.liveWith
          : liveWith // ignore: cast_nullable_to_non_nullable
              as List<User>,
      followingList: followingList == freezed
          ? _value.followingList
          : followingList // ignore: cast_nullable_to_non_nullable
              as List<User>,
      posts: posts == freezed
          ? _value.posts
          : posts // ignore: cast_nullable_to_non_nullable
              as List<Post>,
    ));
  }
}

/// @nodoc
abstract class _$UserCopyWith<$Res> implements $UserCopyWith<$Res> {
  factory _$UserCopyWith(_User value, $Res Function(_User) then) =
      __$UserCopyWithImpl<$Res>;
  @override
  $Res call(
      {String nickname,
      String? profileImage,
      bool hasStory,
      bool isLive,
      List<User> liveWith,
      List<User> followingList,
      List<Post> posts});
}

/// @nodoc
class __$UserCopyWithImpl<$Res> extends _$UserCopyWithImpl<$Res>
    implements _$UserCopyWith<$Res> {
  __$UserCopyWithImpl(_User _value, $Res Function(_User) _then)
      : super(_value, (v) => _then(v as _User));

  @override
  _User get _value => super._value as _User;

  @override
  $Res call({
    Object? nickname = freezed,
    Object? profileImage = freezed,
    Object? hasStory = freezed,
    Object? isLive = freezed,
    Object? liveWith = freezed,
    Object? followingList = freezed,
    Object? posts = freezed,
  }) {
    return _then(_User(
      nickname: nickname == freezed
          ? _value.nickname
          : nickname // ignore: cast_nullable_to_non_nullable
              as String,
      profileImage: profileImage == freezed
          ? _value.profileImage
          : profileImage // ignore: cast_nullable_to_non_nullable
              as String?,
      hasStory: hasStory == freezed
          ? _value.hasStory
          : hasStory // ignore: cast_nullable_to_non_nullable
              as bool,
      isLive: isLive == freezed
          ? _value.isLive
          : isLive // ignore: cast_nullable_to_non_nullable
              as bool,
      liveWith: liveWith == freezed
          ? _value.liveWith
          : liveWith // ignore: cast_nullable_to_non_nullable
              as List<User>,
      followingList: followingList == freezed
          ? _value.followingList
          : followingList // ignore: cast_nullable_to_non_nullable
              as List<User>,
      posts: posts == freezed
          ? _value.posts
          : posts // ignore: cast_nullable_to_non_nullable
              as List<Post>,
    ));
  }
}

/// @nodoc

class _$_User implements _User {
  _$_User(
      {required this.nickname,
      this.profileImage,
      this.hasStory = false,
      this.isLive = false,
      this.liveWith = const [],
      this.followingList = const [],
      this.posts = const []})
      : assert(isLive || liveWith.isEmpty,
            'The user cannot be `liveWith` someone, if he itself `!isLive`'),
        assert(profileImage == null || profileImage.isNotEmpty,
            'Invalid profile image provided');

  @override
  final String nickname;
  @override
  final String? profileImage;
  @JsonKey(defaultValue: false)
  @override
  final bool hasStory;
  @JsonKey(defaultValue: false)
  @override
  final bool isLive;
  @JsonKey(defaultValue: const [])
  @override
  final List<User> liveWith;
  @JsonKey(defaultValue: const [])
  @override
  final List<User> followingList;
  @JsonKey(defaultValue: const [])
  @override
  final List<Post> posts;

  @override
  String toString() {
    return 'User(nickname: $nickname, profileImage: $profileImage, hasStory: $hasStory, isLive: $isLive, liveWith: $liveWith, followingList: $followingList, posts: $posts)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _User &&
            (identical(other.nickname, nickname) ||
                const DeepCollectionEquality()
                    .equals(other.nickname, nickname)) &&
            (identical(other.profileImage, profileImage) ||
                const DeepCollectionEquality()
                    .equals(other.profileImage, profileImage)) &&
            (identical(other.hasStory, hasStory) ||
                const DeepCollectionEquality()
                    .equals(other.hasStory, hasStory)) &&
            (identical(other.isLive, isLive) ||
                const DeepCollectionEquality().equals(other.isLive, isLive)) &&
            (identical(other.liveWith, liveWith) ||
                const DeepCollectionEquality()
                    .equals(other.liveWith, liveWith)) &&
            (identical(other.followingList, followingList) ||
                const DeepCollectionEquality()
                    .equals(other.followingList, followingList)) &&
            (identical(other.posts, posts) ||
                const DeepCollectionEquality().equals(other.posts, posts)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(nickname) ^
      const DeepCollectionEquality().hash(profileImage) ^
      const DeepCollectionEquality().hash(hasStory) ^
      const DeepCollectionEquality().hash(isLive) ^
      const DeepCollectionEquality().hash(liveWith) ^
      const DeepCollectionEquality().hash(followingList) ^
      const DeepCollectionEquality().hash(posts);

  @JsonKey(ignore: true)
  @override
  _$UserCopyWith<_User> get copyWith =>
      __$UserCopyWithImpl<_User>(this, _$identity);
}

abstract class _User implements User {
  factory _User(
      {required String nickname,
      String? profileImage,
      bool hasStory,
      bool isLive,
      List<User> liveWith,
      List<User> followingList,
      List<Post> posts}) = _$_User;

  @override
  String get nickname => throw _privateConstructorUsedError;
  @override
  String? get profileImage => throw _privateConstructorUsedError;
  @override
  bool get hasStory => throw _privateConstructorUsedError;
  @override
  bool get isLive => throw _privateConstructorUsedError;
  @override
  List<User> get liveWith => throw _privateConstructorUsedError;
  @override
  List<User> get followingList => throw _privateConstructorUsedError;
  @override
  List<Post> get posts => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$UserCopyWith<_User> get copyWith => throw _privateConstructorUsedError;
}

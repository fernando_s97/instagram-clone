import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:instagram_clone/app/models/domain/comment.dart';

part 'post.freezed.dart';

@freezed
class Post with _$Post {
  @Assert('mediasUrl.isNotEmpty', 'No media provided. At least 1 is required.')
  @Assert('!mediasUrl.any((url) => url.isEmpty)', 'Invalid media provided.')
  factory Post({
    required List<String> mediasUrl,
    @Default(0) int likes,
    String? caption,
    @Default([]) List<Comment> comments,
  }) = _Post;
}

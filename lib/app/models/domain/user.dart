import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:instagram_clone/app/models/domain/post.dart';

part 'user.freezed.dart';

@freezed
class User with _$User {
  @Assert(
    'isLive || liveWith.isEmpty',
    'The user cannot be `liveWith` someone, if he itself `!isLive`',
  )
  @Assert(
    'profileImage == null || profileImage.isNotEmpty',
    'Invalid profile image provided',
  )
  factory User({
    required String nickname,
    String? profileImage,
    @Default(false) bool hasStory,
    @Default(false) bool isLive,
    @Default([]) List<User> liveWith,
    @Default([]) List<User> followingList,
    @Default([]) List<Post> posts,
  }) = _User;
}

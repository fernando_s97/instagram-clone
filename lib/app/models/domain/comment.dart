import 'package:freezed_annotation/freezed_annotation.dart';

part 'comment.freezed.dart';

@freezed
class Comment with _$Comment {
  factory Comment({
    required String comment,
    @Default(0) int likes,
  }) = _Comment;
}

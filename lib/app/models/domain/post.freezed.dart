// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'post.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$PostTearOff {
  const _$PostTearOff();

  _Post call(
      {required List<String> mediasUrl,
      int likes = 0,
      String? caption,
      List<Comment> comments = const []}) {
    return _Post(
      mediasUrl: mediasUrl,
      likes: likes,
      caption: caption,
      comments: comments,
    );
  }
}

/// @nodoc
const $Post = _$PostTearOff();

/// @nodoc
mixin _$Post {
  List<String> get mediasUrl => throw _privateConstructorUsedError;
  int get likes => throw _privateConstructorUsedError;
  String? get caption => throw _privateConstructorUsedError;
  List<Comment> get comments => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PostCopyWith<Post> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PostCopyWith<$Res> {
  factory $PostCopyWith(Post value, $Res Function(Post) then) =
      _$PostCopyWithImpl<$Res>;
  $Res call(
      {List<String> mediasUrl,
      int likes,
      String? caption,
      List<Comment> comments});
}

/// @nodoc
class _$PostCopyWithImpl<$Res> implements $PostCopyWith<$Res> {
  _$PostCopyWithImpl(this._value, this._then);

  final Post _value;
  // ignore: unused_field
  final $Res Function(Post) _then;

  @override
  $Res call({
    Object? mediasUrl = freezed,
    Object? likes = freezed,
    Object? caption = freezed,
    Object? comments = freezed,
  }) {
    return _then(_value.copyWith(
      mediasUrl: mediasUrl == freezed
          ? _value.mediasUrl
          : mediasUrl // ignore: cast_nullable_to_non_nullable
              as List<String>,
      likes: likes == freezed
          ? _value.likes
          : likes // ignore: cast_nullable_to_non_nullable
              as int,
      caption: caption == freezed
          ? _value.caption
          : caption // ignore: cast_nullable_to_non_nullable
              as String?,
      comments: comments == freezed
          ? _value.comments
          : comments // ignore: cast_nullable_to_non_nullable
              as List<Comment>,
    ));
  }
}

/// @nodoc
abstract class _$PostCopyWith<$Res> implements $PostCopyWith<$Res> {
  factory _$PostCopyWith(_Post value, $Res Function(_Post) then) =
      __$PostCopyWithImpl<$Res>;
  @override
  $Res call(
      {List<String> mediasUrl,
      int likes,
      String? caption,
      List<Comment> comments});
}

/// @nodoc
class __$PostCopyWithImpl<$Res> extends _$PostCopyWithImpl<$Res>
    implements _$PostCopyWith<$Res> {
  __$PostCopyWithImpl(_Post _value, $Res Function(_Post) _then)
      : super(_value, (v) => _then(v as _Post));

  @override
  _Post get _value => super._value as _Post;

  @override
  $Res call({
    Object? mediasUrl = freezed,
    Object? likes = freezed,
    Object? caption = freezed,
    Object? comments = freezed,
  }) {
    return _then(_Post(
      mediasUrl: mediasUrl == freezed
          ? _value.mediasUrl
          : mediasUrl // ignore: cast_nullable_to_non_nullable
              as List<String>,
      likes: likes == freezed
          ? _value.likes
          : likes // ignore: cast_nullable_to_non_nullable
              as int,
      caption: caption == freezed
          ? _value.caption
          : caption // ignore: cast_nullable_to_non_nullable
              as String?,
      comments: comments == freezed
          ? _value.comments
          : comments // ignore: cast_nullable_to_non_nullable
              as List<Comment>,
    ));
  }
}

/// @nodoc

class _$_Post implements _Post {
  _$_Post(
      {required this.mediasUrl,
      this.likes = 0,
      this.caption,
      this.comments = const []})
      : assert(
            mediasUrl.isNotEmpty, 'No media provided. At least 1 is required.'),
        assert(!mediasUrl.any((url) => url.isEmpty), 'Invalid media provided.');

  @override
  final List<String> mediasUrl;
  @JsonKey(defaultValue: 0)
  @override
  final int likes;
  @override
  final String? caption;
  @JsonKey(defaultValue: const [])
  @override
  final List<Comment> comments;

  @override
  String toString() {
    return 'Post(mediasUrl: $mediasUrl, likes: $likes, caption: $caption, comments: $comments)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Post &&
            (identical(other.mediasUrl, mediasUrl) ||
                const DeepCollectionEquality()
                    .equals(other.mediasUrl, mediasUrl)) &&
            (identical(other.likes, likes) ||
                const DeepCollectionEquality().equals(other.likes, likes)) &&
            (identical(other.caption, caption) ||
                const DeepCollectionEquality()
                    .equals(other.caption, caption)) &&
            (identical(other.comments, comments) ||
                const DeepCollectionEquality()
                    .equals(other.comments, comments)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(mediasUrl) ^
      const DeepCollectionEquality().hash(likes) ^
      const DeepCollectionEquality().hash(caption) ^
      const DeepCollectionEquality().hash(comments);

  @JsonKey(ignore: true)
  @override
  _$PostCopyWith<_Post> get copyWith =>
      __$PostCopyWithImpl<_Post>(this, _$identity);
}

abstract class _Post implements Post {
  factory _Post(
      {required List<String> mediasUrl,
      int likes,
      String? caption,
      List<Comment> comments}) = _$_Post;

  @override
  List<String> get mediasUrl => throw _privateConstructorUsedError;
  @override
  int get likes => throw _privateConstructorUsedError;
  @override
  String? get caption => throw _privateConstructorUsedError;
  @override
  List<Comment> get comments => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$PostCopyWith<_Post> get copyWith => throw _privateConstructorUsedError;
}

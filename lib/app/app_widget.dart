import 'dart:math';

import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:instagram_clone/app/models/domain/comment.dart';
import 'package:instagram_clone/app/models/domain/post.dart';
import 'package:instagram_clone/app/models/domain/user.dart';
import 'package:instagram_clone/app/presenter/bottom_nav_bar_container.dart';

class AppWidget extends StatefulWidget {
  const AppWidget({Key? key}) : super(key: key);

  @override
  State<AppWidget> createState() => _AppWidgetState();
}

class _AppWidgetState extends State<AppWidget> {
  late final User _loggedInUser;

  @override
  void initState() {
    super.initState();

    _loggedInUser = _buildLoggedInUser();
  }

  @override
  Widget build(BuildContext context) {
    final darkTheme = ThemeData.dark();
    const blackColor = Colors.black;
    final colorScheme = darkTheme.colorScheme.copyWith(
      secondary: Colors.blue,
      onSecondary: Colors.white,
      background: blackColor,
      onBackground: Colors.white,
    );

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Instagram Clone',
      themeMode: ThemeMode.dark,
      darkTheme: darkTheme.copyWith(
        colorScheme: colorScheme,
        scaffoldBackgroundColor: colorScheme.background,
        appBarTheme: AppBarTheme(
          backgroundColor: colorScheme.background,
        ),
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          backgroundColor: colorScheme.background,
          selectedIconTheme: const IconThemeData(color: Colors.white),
          showSelectedLabels: false,
          showUnselectedLabels: false,
          type: BottomNavigationBarType.fixed,
        ),
        iconTheme: const IconThemeData(
          color: Colors.white,
          size: 24,
        ),
        dividerTheme: const DividerThemeData(
          space: 0,
        ),
      ),
      home: BottomNavBarContainer(loggedInUser: _loggedInUser),
    );
  }

  User _buildLoggedInUser() {
    const profileImageSize = 320;
    final random = Random();
    final faker = Faker();

    return User(
      nickname: faker.internet.userName(),
      profileImage: null,
      hasStory: random.nextBool(),
      isLive: false,
      liveWith: [],
      followingList: List.generate(4, (followerIndex) {
        return User(
          nickname: faker.internet.userName(),
          profileImage:
              'https://picsum.photos/$profileImageSize?id=$followerIndex',
          hasStory: random.nextBool(),
          isLive: random.nextBool(),
          liveWith: [],
          followingList: [],
          posts: List.generate(1, (postIndex) {
            return Post(
              mediasUrl: List.generate(random.nextInt(3) + 1, (mediaIndex) {
                final id = '$followerIndex.$postIndex.$mediaIndex';
                return 'https://picsum.photos/1080?id=$id';
              }),
              caption: faker.lorem.sentence(),
              likes: random.nextInt(1000),
              comments: List.generate(random.nextInt(100), (index) {
                return Comment(
                  comment: faker.lorem.sentence(),
                  likes: random.nextInt(1000),
                );
              }),
            );
          }),
        );
      }),
      posts: [],
    );
  }
}

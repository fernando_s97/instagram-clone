# instagram_clone

A clone of Instagram (app) for study purposes.

# Roadmap

- [x] Improve logo
- [x] Improve icons
- [x] Add splash screen
- [x] Add posts' media count indicator
- [x] Add "live" indicator
- [] Add "live with" indicator
- [] Improve responsiveness